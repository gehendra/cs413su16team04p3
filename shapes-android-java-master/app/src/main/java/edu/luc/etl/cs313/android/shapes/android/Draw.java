package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;


import java.util.ArrayList;
import java.util.List;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME
		this.paint = paint; // FIXME
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		int paint_color = paint.getColor(); // store current paint color
		paint.setColor(c.getColor()); // set paint color to be the one from stroke color
        c.getShape().accept(this); // call visitor
        paint.setColor(paint_color); // assign back the color before stroke color or restore stroke color
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
        Style paint_style = paint.getStyle(); // store current paint style
        paint.setStyle(Style.FILL_AND_STROKE); // set fill and stroke style
        f.getShape().accept(this); // call visitor
        paint.setStyle(paint_style); // assign style back or restore its style
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
        for (Shape s : g.getShapes()){ // Iterate over array list of shapes
            s.accept(this); // call visitor
        }
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
        canvas.translate(l.getX(), l.getY()); // move location
        l.getShape().accept(this); // call visitor
        canvas.translate(-l.getX(), -l.getY()); // move it back to previous location
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint); // Setting up point(x,y) along with width and height for the rectangle shape along with paint object
		return null;
	}

	@Override
	public Void onOutline(Outline o){
        Style style = paint.getStyle(); // store current style
        paint.setStyle(Style.STROKE); // set stroke for the paint
        o.getShape().accept(this); // call visitor
        paint.setStyle(style); // restore style
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
        final float[] pts = new float[(s.getPoints().size() * 4)]; // To draw a line we need two sets of points. Start point and end points. Each points contains x and y co-ordinate. 4 lines * 2 set of points * 2 (x, y)

        int count = -1; // set it to -1 as the increament counter going to add 1 to it
        for (int i=0; i < s.getPoints().size(); i++){
            pts[count += 1] = (float)s.getPoints().get(i).getX();  // Get X of first point and cast it to float from int
            pts[count += 1] = (float)s.getPoints().get(i).getY(); // Get Y of first point and cast it to float from int
            if (i == (s.getPoints().size()-1)){ // if its the last point then get first point co-ordinate so that it can join first point to complete the polygon
                pts[count += 1] = (float)s.getPoints().get(0).getX();
                pts[count += 1] = (float)s.getPoints().get(0).getY();
            }
            else { // get next point x and y
                pts[count += 1] = (float)s.getPoints().get(i + 1).getX(); // Get X of second point
                pts[count += 1] = (float)s.getPoints().get(i + 1).getY(); // Get X of second point
            }
        }

        canvas.drawLines(pts, paint);
        return null;
	}
}
