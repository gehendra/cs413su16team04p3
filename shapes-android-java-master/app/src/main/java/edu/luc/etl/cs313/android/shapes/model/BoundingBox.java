package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this); // Calls visitor that implements shape
	}

	@Override
	public Location onGroup(final Group g) { // this returns the bounding box with x and y co-ordinate along with its width and height
        Location bounding_box = null; // declaring bounding box

        for(Shape current_shape : g.getShapes()){ // iterate over shapes collection
            int width, bounding_box_width, height, bounding_box_height, x, bounding_box_x, y, bounding_box_y;

            Location current_shape_location = current_shape.accept(this); // call visitor
            Rectangle draw_box = (Rectangle)current_shape_location.getShape(); // Type cast shape to Rectangle

            width = bounding_box_width = draw_box.getWidth(); // get width of the current shape after being type cast to Rectangle
            height = bounding_box_height = draw_box.getHeight(); // get height of the current shape after being type cast to Rectangle

            x = bounding_box_x = current_shape_location.getX(); // get X co-ordinate of current shape
            y = bounding_box_y = current_shape_location.getY(); // get Y co-ordinate of current shape

            if (bounding_box != null) {// check for existing bounding box if it has already been set up yet set up update its x and y co-ordinate along with its width and height
                bounding_box_x = bounding_box.getX();
                bounding_box_y = bounding_box.getY();

                bounding_box_width = ((Rectangle)bounding_box.getShape()).getWidth(); // Type cast shape to Rectangle and extract width
                bounding_box_height = ((Rectangle)bounding_box.getShape()).getHeight(); // Type cast shape to Rectangle and extract height
            }

            bounding_box_width = (x < bounding_box_x) ? (bounding_box_width + (bounding_box_x - x)) : bounding_box_width; // checking current shape location x point against bounding box x point. extend bounding box width as its current width needs to extend to capture current shape co-ordinate

            bounding_box_height = (y < bounding_box_y) ? (bounding_box_height + (bounding_box_y - y)) : bounding_box_height; // checking current shape location y point against bounding box y point. extend bounding box height as its current height needs to extend to capture current shape co-ordinate

            // calculate new width of bounding box width if necessary
            bounding_box_width = ((x - bounding_box_x + width) > bounding_box_width) ? (x - bounding_box_x + width) : bounding_box_width;

            // calculate new height of bounding box height if necessary
            bounding_box_height = ((y - bounding_box_y + height) > bounding_box_height) ? (y - bounding_box_y + height) : bounding_box_height;

            bounding_box = new Location(bounding_box_x, bounding_box_y, new Rectangle(bounding_box_width, bounding_box_height)); // set up new location with x and y point, and rectangle box with newly calculated bounding box width and height.
        }

		return bounding_box;
	}



	@Override
	public Location onLocation(final Location l) {
        int x = l.getX(); // determine x point
        int y = l.getY(); // determine y point

        Location location = l.getShape().accept(this); // call visitor
        return new Location(x + location.getX(), y + location.getY(), location.getShape()); // determine new location
	}

	@Override
	public Location onRectangle(final Rectangle r) {
        return new Location(0, 0, r); // instantiate location with x and y coordinate and rectangle object
	}

	@Override
	public Location onStroke(final Stroke c) {
		return c.getShape().accept(this); // Calls visitor that implements shape
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this); // Calls visitor that implements shape
	}

	@Override
	public Location onPolygon(final Polygon s) { // This project is using same polygon points and this one works as points on fourth (x) for width calculation and third one for (y) calculation.
        // TODO for different size polygon we would need to determine largest X and Y of the point, choose those points according to determine width and height
        final Point first_point = (Point) s.getPoints().get(0); // get first co-ordinate x,y for the polygon
        final int polygon_size = s.getPoints().size(); // get length of points list array
        final int width = s.getPoints().get(polygon_size - 1).getX() - first_point.getX(); // determine width by calculating x co-ordinate of last and first points
        final int height = s.getPoints().get(2).getY() - first_point.getY(); // determine height by calculating y co-ordinate of third and first points
        final Rectangle rectangle = new Rectangle(width, height); // instantiate rectangle with calculated width and height
        return  new Location(first_point.getX(), first_point.getY(), rectangle); // instantiate location to return it for location
	}
}
