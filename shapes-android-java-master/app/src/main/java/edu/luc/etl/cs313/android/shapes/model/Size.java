package edu.luc.etl.cs313.android.shapes.model;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Size implements Visitor<Integer> {

	// TODO entirely your job

	@Override
	public Integer onPolygon(final Polygon p) {
		return 1;
	}

	@Override
	public Integer onCircle(final Circle c) {
		return 1;
	}

	@Override
	public Integer onGroup(final Group g) {
        int count = 0; // set size 0 at the beginning
        for (Shape s : g.getShapes()){ // Iterate over shape collection
            count += s.accept(this); // Basically it returns 1 if shape is any of rectangle, polygon or circle
        }
		return count;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {
		return 1;
	}

	@Override
	public Integer onOutline(final Outline o) {
        Shape s = o.getShape();
        return s.accept(this).intValue();
	}

	@Override
	public Integer onFill(final Fill c) {
        Shape s = c.getShape();
        return s.accept(this).intValue();
	}

	@Override
	public Integer onLocation(final Location l) {
        Shape s = l.getShape();
        return s.accept(this).intValue();
	}

	@Override
	public Integer onStroke(final Stroke c) {
        Shape s = c.getShape();
        return s.accept(this).intValue();
	}
}